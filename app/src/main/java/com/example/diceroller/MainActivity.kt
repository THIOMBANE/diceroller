package com.example.diceroller

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button.setOnClickListener {
            rollDice()
        }
    }

    /** version code longue
     * private fun rollDice() {
    val diceRoll = Dice(6).roll()
    when(diceRoll) {
    1 -> imageView.setImageResource(R.drawable.dice_1)
    2 -> imageView.setImageResource(R.drawable.dice_2)
    3 -> imageView.setImageResource(R.drawable.dice_3)
    4 -> imageView.setImageResource(R.drawable.dice_4)
    5 -> imageView.setImageResource(R.drawable.dice_5)
    6 -> imageView.setImageResource(R.drawable.dice_6)
        }
    }
     *
     */
    private fun rollDice() {
      val drawableResource = when (Dice(6).roll()) {
                   1 -> R.drawable.dice_1
                   2 -> R.drawable.dice_2
                   3 -> R.drawable.dice_3
                   4 -> R.drawable.dice_4
                   5 -> R.drawable.dice_5
                   else -> R.drawable.dice_6
         }
        imageView.setImageResource(drawableResource)
        imageView.contentDescription = Dice(6).roll().toString()
    }
}